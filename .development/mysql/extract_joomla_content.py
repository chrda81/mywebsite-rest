#!/usr/bin/python
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# Parser for phpMySQLAdmin XML exports.
import json
import textwrap
from datetime import datetime
from string import Template

from lxml import etree

# // BEGIN Custom configuration
joomla_prefix = "cb6qz"
xmldoc = etree.parse('.development/mysql/joomla_content.xml')
output = '.development/mysql/joomla2django_conv.json'

# mapping from joomla to django category id
#   2: Content => 1: Interne Inhalte
#   11: CP-Online => 2: CP-Online
#   12: CP-Info => 3: CP-Info
#   13: Pressespiegel => 4: Pressespiegel
#   14: Presse Journalisten => 5: Presse Journalisten
#   10: Vorteile => 6: Vorteile
category_ids = {'2': '1', '11': '2', '12': '3', '13': '4', '14': '5', '10': '6'}

article_json = Template("""{
  "model": "blog.article",
  "pk": $id,
  "fields": {
    "category": $category,
    "date": "$date",
    "author": "admin",
    "internal": false,
    "published": $published,
    "archived": $archived,
    "slug": "$slug"
  }
},
{
  "model": "blog.articletranslation",
  "pk": $id,
  "fields": {
    "language_code": "de",
    "title": "$title",
    "body": "$body",
    "teaser": "$teaser",
    "master": $id
  }
},""")

# init value for pk
pk_value = 1

# // END Custom configuration


class safelist(list):
    def get(self, index, default=None):
        try:
            return self.__getitem__(index)
        except IndexError:
            return default


def replace_html(text):
    text = text.replace('src="images/bilder_content/', 'src="/media/camionpro.de/images/internal/')
    text = text.replace('src="images/newsbilder/', 'src="/media/camionpro.de/images/news/')
    text = text.replace('src="images/cp_bilder/', 'src="/media/camionpro.de/images/news/')
    text = text.replace('src="images/cp_logos/', 'src="/media/camionpro.de/images/logos/')
    text = text.replace('src="images/cp_docu/', 'src="/media/camionpro.de/documents/')
    text = text.replace("\"", "\\\"")
    text = text.replace('\r\n', '')
    text = text.replace('\n', '')
    return text


def _main():
    global pk_value  # we have to use the 'global' keyword to increment the global variable
    root = xmldoc.getroot()
    pma_tables = root.xpath('.//pma:table', namespaces=root.nsmap)
    for pma_table in pma_tables:
        # handle table 'cb6qz_content'
        if pma_table.attrib['name'] == ("%s_content" % joomla_prefix):
            with open(output, "w") as text_file:
                print("[", file=text_file)
                tables = root.xpath('.//table[@name="%s_content"]' % joomla_prefix)
                # iterate through all sub table.columns
                for table in tables:
                    # extract value 'alias' from table
                    value_slug = safelist(table.xpath('.//column[@name="alias"]/text()')).get(0, "empty-slug")
                    # extract value 'title' from table and replace necessary string parts
                    value_title = replace_html(safelist(table.xpath(
                        './/column[@name="title"]/text()')).get(0, "Empty title"))
                    # extract value 'fulltext' from table and replace necessary string parts
                    value_body = replace_html(safelist(table.xpath(
                        './/column[@name="fulltext"]/text()')).get(0, "<p>Empty body</p>"))
                    # extract value 'introtext' from table and replace necessary string parts
                    value_teaser = replace_html(safelist(table.xpath(
                        './/column[@name="introtext"]/text()')).get(0, "<p>Empty teaser</p>"))
                    # extract value 'catid' from table and get correct django mapping
                    value_category = category_ids.get(safelist(table.xpath(
                        './/column[@name="catid"]/text()')).get(0, -1), '-1')
                    # exract value 'state' from table and get correct django mapping
                    #    0: inactive => published: false, archived: false
                    #    1: active   => published: true, archived: false
                    #   -2: deleted  => published: false, archived: true
                    value_state = safelist(table.xpath('.//column[@name="state"]/text()')).get(0, '0')
                    if value_state == '0':
                        value_published = False
                        value_archived = False
                    if value_state == '1':
                        value_published = True
                        value_archived = False
                    if value_state == '-2':
                        value_published = False
                        value_archived = True
                    # extract value 'created' from table and apply format YYYY-mm-dd
                    value_date = safelist(table.xpath('.//column[@name="created"]/text()')
                                          ).get(0, "{:%Y-%m-%d}".format(datetime.now()))[:10]

                    print(article_json.substitute(
                        dict(
                            id=pk_value,
                            slug=value_slug,
                            title=value_title,
                            body=value_body,
                            teaser=value_teaser,
                            published=json.dumps(value_published),  # we've to convert python boolean to json boolean
                            archived=json.dumps(value_archived),    # we've to convert python boolean to json boolean
                            category=value_category,
                            date=value_date
                        )
                    ), file=text_file)

                    # incrementing pk
                    pk_value += 1

                print("]", file=text_file)


if __name__ == "__main__":
    _main()
