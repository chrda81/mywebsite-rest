# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------

Website URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

import os

from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.db.utils import OperationalError
from django.urls import include, path, re_path
from django.views.generic import RedirectView
from django.views.i18n import JavaScriptCatalog

import menu
from filebrowser.sites import site
from mywebsite_home import views as homeviews
from mywebsite_members.admin import admin

urlpatterns = [
    re_path(r'^favicon\.ico$', RedirectView.as_view(url='/static/mywebsite/img/default/favicon.gif')),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
    path('qr/', include("django_otp.urls")),  # enable QRcode for 2FA
    path('hitcount/', include('hitcount.urls', namespace='hitcount')),  # for hitcounts built-in AJAX post view
    path('captcha/', include('captcha.urls')),  # for captcha URLS
]

# handle language prefix in URL patterns
try:
    urlpatterns += i18n_patterns(
        path('tinymce/', include('tinymce.urls')),
        path('admin/filebrowser/', site.urls),
        # added custom view, see http://www.beardygeek.com/2010/03/adding-views-to-the-django-admin/
        path('admin/manual/', homeviews.manual, name="admin-manual"),
        path('admin/doc/', include('django.contrib.admindocs.urls')),
        path('admin/', admin.site.urls),
        # added REST framework's API URLS
        path('api/', include('mywebsite_rest.urls')),
        # added javascript translation support, see
        # https://docs.djangoproject.com/en/dev/topics/i18n/translation/#internationalization-in-javascript-code
        path('jsi18n/', JavaScriptCatalog.as_view(), name='javascript-catalog'),
        # added website modules
        path('', include('mywebsite_home.urls', namespace='home')),
        path('blog/', include('mywebsite_blog.urls', namespace='blog')),
        path('members/', include('mywebsite_members.urls', namespace='members')),
        prefix_default_language=False
    )
except OperationalError:
    # happens when db doesn't exist yet, views.py should be importable without this side effect
    pass

# serve media files during development
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=os.path.join(settings.BASE_DIR, 'static'))
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


@classmethod
def load_menus(c):
    """
    load_menus loads the menu.py file from test_website.
    """

    # we don't need to do this more than once
    if c.loaded:
        return

    try:
        __import__('test_website.menus', fromlist=["menu", ])
    except ImportError:
        pass

    c.loaded = True


# override menu.Menu.load_menus with our own method, to load custom_website.menus instead
menu.Menu.load_menus = load_menus
