# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import json
import os

import setuptools
from setuptools.command.sdist import sdist


# Override build command
class BuildCommand(sdist):

    def run(self):
        # Run the original build command
        sdist.run(self)
        # os.system('rm -vrf ./build ./dist ./*.pyc ./*.tgz ./*.egg-info')
        os.system('rm -vrf ./*.egg-info')


with open('package.json') as f:
    package_json = json.load(f)

with open("README.md", "r") as fh:
    long_description = fh.read()


def read_requirements(requirements):
    """Parse requirements from requirements.txt."""
    with open(requirements, 'r') as f:
        reqs = [line.rstrip() for line in f]
    return reqs


setuptools.setup(
    name=package_json["name"],
    version=package_json["version"],
    author=package_json["author"],
    author_email=package_json["author_email"],
    description=package_json["description"],
    long_description=long_description,
    long_description_content_type="text/markdown",
    url=package_json["homepage"],
    packages=setuptools.find_packages(exclude=["tests"]),
    include_package_data=True,
    install_requires=read_requirements("requirements/production.txt"),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: German Free Software License 1.0",
        "Operating System :: OS Independent",
    ],
    license=package_json["license"],
    cmdclass={
        'sdist': BuildCommand,
    }
)
