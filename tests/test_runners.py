# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.core import management
from django.test.runner import DiscoverRunner


class NoDbTestRunner(DiscoverRunner):

    def setup_databases(self, **kwargs):
        """ Override the database creation defined in parent class """
        pass

    def teardown_databases(self, old_config, **kwargs):
        """ Override the database teardown defined in parent class """
        pass


class DefaultDbTestRunner(DiscoverRunner):

    def setup_databases(self, **kwargs):
        """ Extend the database creation defined in parent class """
        super().setup_databases(**kwargs)
        # load sample fixtures
        management.call_command('loaddata', 'sample_data.json', verbosity=0)

    def teardown_databases(self, old_config, **kwargs):
        """ Extend the database teardown defined in parent class """
        old_config = super().setup_databases(**kwargs)
        super().teardown_databases(old_config, **kwargs)
