/*--------------------------------------------------------------
 *  Copyright (C) 2018 dsoft-app-dev.de and friends.
 *
 *  This Program may be used by anyone in accordance with the terms of the
 *  German Free Software License
 *
 *  The License may be obtained under http://www.d-fsl.org.
 *-------------------------------------------------------------*/

'use strict';

QUnit.test('getSettingsValue basics', function(assert) {
    var key = 'force_script_name';
    assert.equal(readKeyValuesFromCookie(key), '');
});
