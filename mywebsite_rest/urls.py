# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.urls import include, path

from rest_framework import routers

from .views import (
    APIRootView, ArticleViewSet, CategoryViewSet, NewsViewSet, TokenObtainPairView, TokenRefreshView, TokenVerifyView,
    TranslationViewSet, UserActivationView, UserViewSet, set_language_jx
)


class Router(routers.DefaultRouter):
    include_root_view = True
    include_format_suffixes = False
    root_view_name = 'index'

    def get_api_root_view(self, api_urls=None):
        return APIRootView.as_view()


# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()  # = Router()
router.register(r'news', NewsViewSet)
router.register(r'articles', ArticleViewSet)
router.register(r'categories', CategoryViewSet)
router.register(r'translation', TranslationViewSet, basename='translation')

# TODO: Add as nested router
auth_router = routers.DefaultRouter()  # = Router()
auth_router.register(r'users', UserViewSet)

urlpatterns = [
    # url for changing the language
    path('v1/set_language/', set_language_jx, name='set_language_jx'),
    # The endpoints of the routers
    path('v1/', include(router.urls)),
    # The endpoints of the auth routers
    path('v1/auth/', include(auth_router.urls)),
    # djoser auth urls
    path('v1/auth/', include('djoser.urls.authtoken')),
    path('v1/auth/users/activate/<str:uid>/<str:token>/', UserActivationView.as_view()),
    # JWT auth urls
    path('v1/auth/obtain-token/', TokenObtainPairView.as_view(), name='token-obtain-pair'),
    path('v1/auth/refresh-token/', TokenRefreshView.as_view(), name='token-refresh'),
    path('v1/auth/verify-token/', TokenVerifyView.as_view(), name='token-refresh'),
]
