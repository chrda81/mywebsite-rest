# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from rest_framework import permissions


class IsAccountOwnerOrAdmin(permissions.BasePermission):
    def has_object_permission(self, request, view, account):
        if request.user:
            return account == request.user or request.user.is_superuser
        return False

    # def has_permission(self, request, view):
    #     # print(view.kwargs)
    #     try:
    #         user_profile = Profile.objects.get(
    #             pk=view.kwargs['pk'])
    #     except:
    #         return False

    #     if request.user.profile == user_profile:
    #         return True

    #     return False
