��          �            x  !   y  
   �  	   �     �     �     �     �     �  	          
     +   $  2   P     �  )   �  B  �  8     
   D     O  
   T     _     g     t  	   �     �     �     �  0   �  8   �  $     /   4                         	                             
                         Cannot set language to locale %s. MENU_ABOUT MENU_BLOG MENU_CATEGORIES MENU_CONTACT MENU_CROWDFUNDING MENU_DATA_PRIVACY MENU_DISCLOSURES MENU_HOME MENU_MEMBERS MENU_TERMS No account found with the given credentials No active account found with the given credentials Set language to locale %s. User with email %(email)s already exists! Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 Sprache konnte nicht zu Ländercode %s geändert werden. Über Mich Blog Kategorien Kontakt Crowdfunding Datenschutz Impressum Home Mitgliederbereich AGB Keinen Account mit diesen Anmeldedaten gefunden. Keinen aktiven Account mit diesen Anmeldedaten gefunden. Sprache zu Ländercode %s geändert. Benutzer mit Email %(email)s existiert bereits! 