# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import sys
from importlib import reload

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test.utils import override_settings
from django.urls import reverse

from rest_framework import status
from rest_framework.test import APITestCase

try:
    from unittest import mock
except ImportError:
    import mock

# User model alias
User = get_user_model()


class AccountTests(APITestCase):
    def setUp(self):
        super(AccountTests, self).setUp()

    @mock.patch(
        "mywebsite_rest.serializers.UserCreateSerializer.Meta.fields",
        ('id', 'nr', 'uuid', 'password', 'company_name', 'title', 'firstName', 'lastName', 'website',
         'email', 'mobile', 'phone', 'fax', 'country', 'zipcode', 'location', 'address', 'active'),
    )
    @override_settings(DJOSER=dict(settings.DJOSER, **{"LOGIN_FIELD": 'email'}))
    def test_create_account_via_email(self):
        """
        Ensure we can create a new account object. We must patch the UserCreateSerializer.Meta.fields with the correct
        setting, because override_settings won't do that for us at this point of runtime.
        """
        url = '/api/v1/auth/users/'
        data = {
            'email': 'test@user.com',
            'password': '123+login!789'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(settings.DJOSER['USERNAME_PREFIX'] in User.objects.get().username)

    @mock.patch(
        "mywebsite_rest.serializers.UserCreateSerializer.Meta.fields",
        ('id', 'nr', 'uuid', 'password', 'company_name', 'title', 'firstName', 'lastName', 'website',
         'email', 'mobile', 'phone', 'fax', 'country', 'zipcode', 'location', 'address', 'active'),
    )
    @override_settings(DJOSER=dict(settings.DJOSER, **{"LOGIN_FIELD": 'username'}))
    def test_create_account_via_username(self):
        """
        Ensure we can create a new account object. We must patch the UserCreateSerializer.Meta.fields with the correct
        setting, because override_settings won't do that for us at this point of runtime.
        """
        url = '/api/v1/auth/users/'
        data = {
            'username': 'LoginUser',
            'password': '123+login!789'
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.get().username, 'LoginUser')
