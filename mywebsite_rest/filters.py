# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2021 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from rest_framework.filters import OrderingFilter


# Work around DRF issue #6886 by always adding the primary key as last order field.
# See https://github.com/encode/django-rest-framework/issues/6886
class StableOrderingFilter(OrderingFilter):
    def get_ordering(self, request, queryset, view):
        # get ordering by query params
        ordering = super(StableOrderingFilter, self).get_ordering(request, queryset, view)
        pk = queryset.model._meta.pk.name

        if ordering is None and hasattr(queryset, 'query'):
            # get ordering by queryset
            orderBy = list(queryset.query.order_by)
            if len(orderBy):
                ordering = orderBy

        if ordering is None:
            # get ordering by model
            ordering = queryset.model._meta.ordering

        if ordering is None:
            # fallback
            return ('-' + pk, )

        return list(ordering) + ['-' + pk]
