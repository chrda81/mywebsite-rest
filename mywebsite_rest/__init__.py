# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

# this version variable will be used in setup.py and other mywebsite modules within this project
__version__ = '1.2.0'
default_app_config = 'mywebsite_rest.apps.RestConfig'
