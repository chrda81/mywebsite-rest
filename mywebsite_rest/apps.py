# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class RestConfig(AppConfig):
    """The default AppConfig for the website module ``mywebsite_rest``."""
    name = 'mywebsite_rest'
    verbose_name = _("Rest")

    def ready(self):
        """Wire up the signals and apply monkey patches """
        import mywebsite_rest.signals
        import mywebsite_rest.monkey_patches.rest_framework  # monkey-patch the classes from rest_framework.serializers
