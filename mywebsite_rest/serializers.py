# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core import exceptions as django_exceptions
from django.db import IntegrityError, transaction
from django.utils.translation import gettext_lazy as _

from djoser import serializers as djoser_serializers
from djoser.conf import settings
from mywebsite_blog.models import Article, Category
from mywebsite_home.models import News
from mywebsite_members.models import Profile
from parler_rest.serializers import TranslatableModelSerializer, TranslatedFieldsField
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.settings import api_settings as drf_settings
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.serializers import PasswordField
from rest_framework_simplejwt.settings import api_settings as jwt_settings
from rest_framework_simplejwt.tokens import RefreshToken, UntypedToken
from rest_framework_simplejwt.utils import datetime_to_epoch
from six import text_type

from .mixins import FlatTranslatedSerializerMixin
from .utils import generate_unique_username

# User model alias
User = get_user_model()


# Serializers define the API representation.
class NewsSerializer(FlatTranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=News)
    date = serializers.DateField(format='%d %b %Y')
    slug = serializers.CharField(default='')
    category = serializers.CharField(default='')
    teaser = serializers.CharField(default='')

    class Meta:
        model = News
        fields = ('id', 'date', 'slug', 'category', 'teaser', 'is_body_title_hidden', 'is_published', 'is_archived', 'translations')


class HitsRelatedField(serializers.RelatedField):
    """
    A custom field to use for the `tagged_object` generic relationship.
    """

    def to_representation(self, value):
        """
        Serialize hits objects to a simple textual representation.
        """

        try:
            obj = value.get()
            return obj.hits
        except:
            return 0


class ArticleSerializer(FlatTranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Article)
    category = serializers.CharField(source='category.name')
    date = serializers.DateField(format='%d %b %Y')
    hits = HitsRelatedField(read_only=True)

    class Meta:
        model = Article
        fields = ('id', 'category', 'slug', 'date', 'author', 'hits', 'is_body_title_hidden',
                  'is_internal', 'is_published', 'is_archived', 'is_onnewsfeed', 'translations')

    def validate(self, data):
        """
        Handler for category field relation
        """
        category_id = int(data.get('category', None).get('name', None))
        if category_id:
            category = Category.objects.get(id=category_id)
            if not category:
                self.fail('cannot_validate_article')

        data.update({'category': category})

        return data


class CategorySerializer(FlatTranslatedSerializerMixin, TranslatableModelSerializer):
    translations = TranslatedFieldsField(shared_model=Category)
    date = serializers.DateField(format='%d %b %Y')
    articles = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'slug', 'date', 'articles', 'is_internal', 'is_published', 'is_archived', 'translations')

    def get_articles(self, obj):
        articles = Article.objects.filter(category=obj, is_archived=False)
        return articles.count()


class TokenSerializer(serializers.ModelSerializer):
    """ extending the serializer class from djoser """

    auth_token = serializers.CharField(source='key')

    class Meta:
        model = settings.TOKEN_MODEL
        fields = ('auth_token', 'user_id',)


class TokenObtainSerializer(serializers.Serializer):
    """ extending the serializer class from rest_framework_simplejwt """

    login_field = settings.LOGIN_FIELD

    def __init__(self, *args, **kwargs):
        super(TokenObtainSerializer, self).__init__(*args, **kwargs)

        self.fields[self.login_field] = serializers.CharField()
        self.fields['password'] = PasswordField()

    def validate(self, attrs):
        try:
            if settings.LOGIN_FIELD == 'email':
                self.user = User.objects.get(email=attrs[self.login_field])
            else:
                self.user = User.objects.get(username=attrs[self.login_field])
        except User.DoesNotExist:
            raise serializers.ValidationError(
                _('No account found with the given credentials'),
            )

        # Prior to Django 1.10, inactive users could be authenticated with the
        # default `ModelBackend`.  As of Django 1.10, the `ModelBackend`
        # prevents inactive users from authenticating.  App designers can still
        # allow inactive users to authenticate by opting for the new
        # `AllowAllUsersModelBackend`.  However, we explicitly prevent inactive
        # users from authenticating to enforce a reasonable policy and provide
        # sensible backwards compatibility with older Django versions.
        if self.user is None or not self.user.is_active or not self.user.check_password(attrs['password']):
            raise serializers.ValidationError(
                _('No active account found with the given credentials'),
            )

        return {}

    @classmethod
    def get_token(cls, user):
        raise NotImplementedError('Must implement `get_token` method for `TokenObtainSerializer` subclasses')


class TokenObtainPairSerializer(TokenObtainSerializer):
    """ extending the serializer class from rest_framework_simplejwt """

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

    def validate(self, attrs):
        data = super(TokenObtainPairSerializer, self).validate(attrs)

        refresh = self.get_token(self.user)

        data['refresh'] = text_type(refresh)
        data['refresh_expires_in'] = refresh.payload['exp'] - datetime_to_epoch(refresh.current_time)
        data['access'] = text_type(refresh.access_token)
        data['access_expires_in'] = refresh.access_token.payload['exp'] - \
            datetime_to_epoch(refresh.access_token.current_time)
        data['user_id'] = self.user.pk

        return data


class TokenRefreshSerializer(serializers.Serializer):
    refresh = serializers.CharField()

    def validate(self, attrs):
        refresh = RefreshToken(attrs['refresh'])

        data = {'access': text_type(refresh.access_token)}
        data['access_expires_in'] = refresh.access_token.payload['exp'] - \
            datetime_to_epoch(refresh.access_token.current_time)

        if jwt_settings.ROTATE_REFRESH_TOKENS:
            if jwt_settings.BLACKLIST_AFTER_ROTATION:
                try:
                    # Attempt to blacklist the given refresh token
                    refresh.blacklist()
                except AttributeError:
                    # If blacklist app not installed, `blacklist` method will
                    # not be present
                    pass

            refresh.set_jti()
            refresh.set_exp()

            data['refresh'] = text_type(refresh)
            data['refresh_expires_in'] = refresh.payload['exp'] - datetime_to_epoch(refresh.current_time)

        return data


class TokenVerifySerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate(self, attrs):
        verify = UntypedToken(attrs['token'])

        data = {'token': attrs['token']}
        data['token_type'] = verify.payload['token_type']
        data['expires_in'] = verify.payload['exp'] - datetime_to_epoch(verify.current_time)

        return data


class UserSerializer(djoser_serializers.UserSerializer):
    """ extending the serializer class from djoser """

    nr = serializers.CharField(read_only=True, source='profile.nr')
    uuid = serializers.CharField(read_only=True, source='profile.uuid')
    company_name = serializers.CharField(read_only=True, source='profile.company_name')
    title = serializers.CharField(read_only=True, source='profile.title')
    firstName = serializers.CharField(read_only=True, source='first_name')
    lastName = serializers.CharField(read_only=True, source='last_name')
    website = serializers.CharField(read_only=True, source='profile.website')
    mobile = PhoneNumberField(read_only=True, source='profile.mobile')
    phone = PhoneNumberField(read_only=True, source='profile.phone')
    fax = PhoneNumberField(read_only=True, source='profile.fax')
    country = serializers.CharField(read_only=True, source='profile.country')
    zipcode = serializers.CharField(read_only=True, source='profile.zipcode')
    location = serializers.CharField(read_only=True, source='profile.location')
    address = serializers.CharField(read_only=True, source='profile.address')
    active = serializers.BooleanField(read_only=True, source='is_active')

    class Meta:
        model = User
        fields = ('id', 'nr', 'uuid', 'password', 'company_name', 'title', 'firstName', 'lastName', 'website',
                  'email', 'mobile', 'phone', 'fax', 'country', 'zipcode', 'location', 'address', 'active')
        extra_kwargs = {
            'password': {'write_only': True, 'required': False}
        }

    def __init__(self, *args, **kwargs):
        super(UserSerializer, self).__init__(*args, **kwargs)

        if settings.TOKEN_MODEL == 'rest_framework.authtoken.models.Token':
            self.fields['token'] = serializers.CharField(read_only=True, source='auth_token.key')

    def update(self, instance, validated_data):
        request = self.context.get('request')
        profile_data = {}

        for key in request.data:
            # get serializers source field
            source_field = self.fields[key].source

            if 'profile.' not in source_field:
                if key not in validated_data:
                    # add missing instance fields to validated_data
                    validated_data[source_field] = request.data[key]
            else:
                # add all other fields to profile_data
                profile_data[source_field.replace('profile.', '')] = request.data[key]

        if profile_data:
            # save profile data
            for field, value in profile_data.items():
                setattr(instance.profile, field, value)
            instance.profile.save(update_fields=profile_data.keys())

        if 'password' in validated_data:
            password = validated_data.pop('password')
            validate_password(password)
            instance.set_password(password)
            instance.save()

        return super(UserSerializer, self).update(instance, validated_data)


class UserCreateSerializer(djoser_serializers.UserCreateSerializer):
    """ extending the serializer class from djoser """

    nr = serializers.CharField(read_only=True, source='profile.nr')
    uuid = serializers.CharField(read_only=True, source='profile.uuid')
    company_name = serializers.CharField(required=False, allow_null=True, source='profile.company_name')
    title = serializers.CharField(required=False, allow_null=True, source='profile.title')
    firstName = serializers.CharField(required=False, allow_null=True, source='first_name')
    lastName = serializers.CharField(required=False, allow_null=True, source='last_name')
    website = serializers.CharField(required=False, allow_null=True, source='profile.website')
    mobile = PhoneNumberField(required=False, allow_null=True, source='profile.mobile')
    phone = PhoneNumberField(required=False, allow_null=True, source='profile.phone')
    fax = PhoneNumberField(required=False, allow_null=True, source='profile.fax')
    country = serializers.CharField(required=False, allow_null=True, source='profile.country')
    zipcode = serializers.CharField(required=False, allow_null=True, source='profile.zipcode')
    location = serializers.CharField(required=False, allow_null=True, source='profile.location')
    address = serializers.CharField(required=False, allow_null=True, source='profile.address')
    active = serializers.BooleanField(read_only=True, source='is_active')

    default_error_messages = {"cannot_create_user": _("Cannot create user!")}

    class Meta:
        model = User
        fields = ('id', 'nr', 'uuid', settings.LOGIN_FIELD, 'password', 'company_name', 'title', 'firstName', 'lastName',
                  'website', 'email', 'mobile', 'phone', 'fax', 'country', 'zipcode', 'location', 'address', 'active')
        extra_kwargs = {
            'password': {'write_only': True, 'required': True}
        }

    def __init__(self, *args, **kwargs):
        super(UserCreateSerializer, self).__init__(*args, **kwargs)

        if settings.TOKEN_MODEL == 'rest_framework.authtoken.models.Token':
            self.fields['token'] = serializers.CharField(read_only=True, source='auth_token.key')

        if JWTAuthentication in drf_settings.DEFAULT_AUTHENTICATION_CLASSES and settings.TOKEN_MODEL is None:
            # extend ModelSerializer with additional fields for JWT Token
            self.token_serializer = TokenObtainPairSerializer()

            self.fields['refresh'] = serializers.SerializerMethodField()
            self.fields['refresh_expires_in'] = serializers.SerializerMethodField()
            self.fields['access'] = serializers.SerializerMethodField()
            self.fields['access_expires_in'] = serializers.SerializerMethodField()

        if settings.LOGIN_FIELD == 'email':
            self.Meta.extra_kwargs.update({'username': {'required': False}, 'email': {'required': True}})
        else:
            self.Meta.extra_kwargs.update({'username': {'required': True}, 'email': {'required': False}})

    def validate(self, attrs):
        profile_data = None
        if 'profile' in attrs.keys():
            # remove profile data for validation, because profile doesn't exist yet
            profile_data = attrs.pop('profile')
        user = User(**attrs)
        password = attrs.get('password')

        if settings.LOGIN_FIELD == 'email':
            # does a user with given email exists?
            email = attrs.get('email', '')
            existingUser = User.objects.filter(email=email)
            if existingUser:
                raise serializers.ValidationError(_('User with email %(email)s already exists!') % {'email': email})

        try:
            validate_password(password, user)
        except django_exceptions.ValidationError as e:
            serializer_error = serializers.as_serializer_error(e)
            raise serializers.ValidationError(
                {'password': serializer_error['non_field_errors']}
            )

        if JWTAuthentication in drf_settings.DEFAULT_AUTHENTICATION_CLASSES and settings.TOKEN_MODEL is None:
            self.token = self.token_serializer.get_token(user)

        if profile_data:
            # append stripped profile data, so it can be handled by perform_create
            attrs.update({'profile': profile_data})
        return attrs

    def create(self, validated_data):
        try:
            user = self.perform_create(validated_data)
        except IntegrityError:
            self.fail('cannot_create_user')

        return user

    def perform_create(self, validated_data):
        with transaction.atomic():
            profile_data = None
            if 'profile' in validated_data.keys():
                # strip profile data
                profile_data = validated_data.pop('profile')
            data = dict(validated_data)

            if settings.LOGIN_FIELD == 'email':
                # auto-generate username
                first_name = data.get('first_name', '')
                last_name = data.get('last_name', '')
                email = data.get('email', '')
                username = data.get('username', '')
                data.update({'username': generate_unique_username([
                    first_name,
                    last_name,
                    email,
                    username,
                    settings.USERNAME_PREFIX])})

            user = User.objects.create_user(**data)

            if profile_data:
                # save profile data
                for field, value in profile_data.items():
                    setattr(user.profile, field, value)
                user.profile.save(update_fields=profile_data.keys())

            if settings.SEND_ACTIVATION_EMAIL:
                user.is_active = False
                user.save(update_fields=['is_active'])
        return user

    def get_refresh(self, obj):
        return text_type(self.token)

    def get_refresh_expires_in(self, obj):
        return self.token.payload['exp'] - datetime_to_epoch(self.token.current_time)

    def get_access(self, obj):
        return text_type(self.token.access_token)

    def get_access_expires_in(self, obj):
        return self.token.access_token.payload['exp'] - datetime_to_epoch(self.token.access_token.current_time)
