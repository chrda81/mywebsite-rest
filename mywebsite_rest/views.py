# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from collections import OrderedDict
from datetime import datetime
from itertools import chain
from operator import attrgetter

from django.conf import settings as django_settings
from django.contrib.auth.tokens import default_token_generator
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import render
from django.template import Context, Template, loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import LANGUAGE_SESSION_KEY, activate, get_language_from_request
from django.utils.translation import gettext as _

import requests
from djoser import views as djoser_views
from djoser.compat import get_user_email
from djoser.conf import settings
from hitcount.models import HitCount
from hitcount.views import HitCountMixin
from mywebsite_blog.models import Article, Category
from mywebsite_home.models import News
from mywebsite_home.templatetags.websettings_tags import transFromWebSettings
from rest_framework import permissions, status, viewsets
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.response import Response
from rest_framework.reverse import NoReverseMatch, reverse
from rest_framework.views import APIView
from rest_framework_simplejwt import views as jwt_views
from reversion.views import RevisionMixin

from . import serializers
from .utils import ListAsQuerySet, str2bool


class APIRootView(APIView):
    _ignore_model_permissions = True
    exclude_from_schema = True

    def get(self, request, *args, **kwargs):
        ret = OrderedDict()
        namespace = request.resolver_match.namespace
        for key, url_name in self.get_api_root_dict(request).items():
            if namespace:
                url_name = namespace + ':' + url_name
            try:
                ret[key] = reverse(
                    url_name,
                    args=args,
                    kwargs=kwargs,
                    request=request,
                    format=kwargs.get('format', None)
                )
            except NoReverseMatch:
                continue

        return Response(ret)

    def get_api_root_dict(self, request):
        api_root_dict = OrderedDict()

        try:
            from . import urls
            list_name = urls.router.routes[0].name
            for prefix, viewset, basename in urls.router.registry:
                # permissions filter
                if not request.user.is_staff and permissions.IsAdminUser in viewset.permission_classes:
                    continue
                api_root_dict[prefix] = list_name.format(basename=basename)
        except ImportError:
            pass

        return api_root_dict


class NewsViewSet(RevisionMixin, viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = serializers.NewsSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        lang_code = get_language_from_request(self.request)

        # standard filter for News
        news_queryset = self.queryset.language(lang_code).filter(is_published=True, is_archived=False)
        # addidtional filter for Articles, which should be shown in news list
        article_queryset = Article.objects.language(lang_code).filter(is_onnewsfeed=True,
                                                                      is_internal=False,
                                                                      is_published=True,
                                                                      is_archived=False)
        # combine both querysets and sort with date desc
        queryset = ListAsQuerySet(sorted(chain(news_queryset, article_queryset),
                                         key=attrgetter('date'), reverse=True), model=News)

        # show filtered categories
        return queryset


class ArticleViewSet(RevisionMixin, HitCountMixin, viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = serializers.ArticleSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        lang_code = get_language_from_request(self.request)

        # standard filter for Article
        queryset = self.queryset.language(lang_code).filter(is_published=True, is_archived=False).order_by('-date')

        # show articles by request
        if 'categoryId' in self.request.query_params:
            categoryId = self.request.query_params.get('categoryId')
            queryset = queryset.filter(category__pk=categoryId)

        if 'category' in self.request.query_params:
            category = self.request.query_params.get('category')
            queryset = queryset.filter(category__slug=category)

        if 'tagname' in self.request.query_params:
            tagname = self.request.query_params.get('tagname')
            queryset = queryset.filter(translations__language_code=lang_code, translations__tags__icontains=tagname)

        if 'isInternal' in self.request.query_params:
            isInternal = self.request.query_params.get('isInternal')
            queryset = queryset.filter(is_internal=str2bool(isInternal))

        if 'isOnNewsFeed' in self.request.query_params:
            isOnNewsFeed = self.request.query_params.get('isOnNewsFeed')
            queryset = queryset.filter(is_onnewsfeed=str2bool(isOnNewsFeed))

        # show filtered articles
        return queryset

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()

        # set hitcount for article
        try:
            hitcount = HitCount.objects.get(object_pk=self.kwargs.get('pk'))
        except HitCount.DoesNotExist:
            content_type = ContentType.objects.get(model=instance._meta.model_name)
            hitcount = HitCount.objects.create(
                hits=0, modified=datetime.now(), content_type=content_type, object_pk=self.kwargs.get('pk')
            )
        hit_count_response = self.hit_count(request, hitcount)

        # get serializer's data and appand hitcount response data
        serializer = self.get_serializer(instance)
        resp = serializer.data
        resp.update(dict(hit_count_response._asdict()))
        return Response(resp)


class CategoryViewSet(RevisionMixin, viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        lang_code = get_language_from_request(self.request)

        # standard filter for Category
        queryset = self.queryset.filter(is_internal=False, is_published=True, is_archived=False)
        # .order_by('translations__name') doesn't work, because it multiplies entries by using .active_translations()
        queryset = ListAsQuerySet(sorted(queryset, key=attrgetter('name'), reverse=False), model=Category)

        # show categories by request
        if 'isInternal' in self.request.query_params:
            isInternal = self.request.query_params.get('isInternal')
            queryset = queryset.filter(is_internal=str2bool(isInternal))

        if 'isPublished' in self.request.query_params:
            isPublished = self.request.query_params.get('isPublished')
            queryset = queryset.filter(is_published=str2bool(isPublished))

        if 'isArchived' in self.request.query_params:
            isArchived = self.request.query_params.get('isArchived')
            queryset = queryset.filter(is_archived=str2bool(isArchived))

        # show filtered categories
        return queryset


class UserViewSet(djoser_views.UserViewSet):
    """ extending the view class from djoser """

    @action(["post"], detail=False)
    def reset_password(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.get_user()

        if user:
            subject_template_name = 'registration/password_reset_subject.txt',
            email_template_name = 'registration/password_reset_email.html',

            context = {
                'domain': Site.objects.get_current().domain,
                'site_name': Site.objects.get_current().name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'user': user,
                'token': default_token_generator.make_token(user),
                'protocol': 'https' if request.is_secure() else 'http',
            }

            """
            Send a django.core.mail.EmailMultiAlternatives to `to_email`.
            """
            subject = loader.render_to_string(subject_template_name, context)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            body = loader.render_to_string(email_template_name, context)
            from_email = django_settings.REGISTRATION_SITE_USER_EMAIL,
            to_email = [get_user_email(user)]

            email_message = EmailMultiAlternatives(subject, body, from_email, to_email)
            email_message.send()

        return Response(status=status.HTTP_204_NO_CONTENT)


class UserActivationView(APIView):
    def get(self, request, uid, token):
        protocol = 'https://' if request.is_secure() else 'http://'
        web_url = protocol + request.get_host()
        post_url = web_url + "/api/v1/auth/users/activation/"
        post_data = {'uid': uid, 'token': token}
        result = requests.post(post_url, data=post_data)
        content = result.text
        args = {
            'user': request.user,
        }
        # Ok code returned by UserViewSet.activation()
        if result.status_code == status.HTTP_204_NO_CONTENT:
            return render(request, 'registration/activation_complete.html', args)
        else:
            return render(request, 'registration/activate.html', args)


class TokenObtainPairView(jwt_views.TokenViewBase):
    """
    Takes a set of user credentials and returns an access and refresh JSON web
    token pair to prove the authentication of those credentials.
    """
    serializer_class = serializers.TokenObtainPairSerializer


class TokenRefreshView(jwt_views.TokenViewBase):
    """
    Takes a refresh type JSON web token and returns an access type JSON web
    token if the refresh token is valid.
    """
    serializer_class = serializers.TokenRefreshSerializer


class TokenVerifyView(jwt_views.TokenViewBase):
    """
    Takes a token and indicates if it is valid.  This view provides no
    information about a token's fitness for a particular use.
    """
    serializer_class = serializers.TokenVerifySerializer


class TranslationViewSet(viewsets.GenericViewSet):
    permission_classes = [permissions.AllowAny]

    def list(self, request):
        lang_code = get_language_from_request(request)

        if lang_code:
            # Switch language for current thread
            activate(lang_code)

        # return translated message
        translated_msgid = None
        msgid = request.GET.get('msgid', None)
        config = request.GET.get('config', None)
        if msgid:
            translated_msgid = transFromWebSettings(msgid)

        if config:
            # Evaluate 'config' string to Context dictionary and render Template
            translated_msgid = Template(translated_msgid).render(Context(eval(config)))

        return Response(
            {
                'msgid': msgid,
                'config': config,
                'lang_code': lang_code,
                'translation': translated_msgid
            }
        )


@api_view(['POST'])
@permission_classes([permissions.AllowAny])
def set_language_jx(request):
    lang_code = request.POST.get('language', None)
    if not lang_code:
        lang_code = get_language_from_request(request)

        if lang_code:
            if hasattr(request, 'session'):
                request.session[LANGUAGE_SESSION_KEY] = lang_code
            response = Response(data={'message': _("Set language to locale %s.") % lang_code, 'success': True},
                                status=status.HTTP_202_ACCEPTED)
            # Always set cookie
            response.set_cookie(
                django_settings.LANGUAGE_COOKIE_NAME, lang_code,
                max_age=django_settings.LANGUAGE_COOKIE_AGE,
                path=django_settings.LANGUAGE_COOKIE_PATH,
                domain=django_settings.LANGUAGE_COOKIE_DOMAIN,
            )
            # Switch language for current thread
            activate(lang_code)

            return response

    return Response(data={'message': _("Cannot set language to locale %s.") % lang_code, 'success': False},
                    status=status.HTTP_417_EXPECTATION_FAILED)
