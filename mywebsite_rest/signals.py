# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

from django.db.models.signals import pre_save
from django.dispatch import receiver

from djoser import signals as djoser_signals
from djoser.conf import settings
from mywebsite.utils.tools import unique_slug_generator
from mywebsite_blog.models import Article, Category
from registration import signals as registration_signals


# subscribe to event pre_save of table 'Article' and 'Categories' to create a slug
@receiver(pre_save, sender=Article)
@receiver(pre_save, sender=Category)
def pre_save_create_slug_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)


def create_user_token(sender, user, request, **kwargs):
    # automatically create token for TokenAuthentication
    token_model = settings.TOKEN_MODEL

    if token_model:
        token = token_model.objects.create(user=user)


if settings.SEND_ACTIVATION_EMAIL:
    djoser_signals.user_activated.connect(create_user_token)
else:
    djoser_signals.user_registered.connect(create_user_token)
registration_signals.user_activated.connect(create_user_token)
