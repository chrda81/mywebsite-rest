"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2020 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import random
import re
import string
import unicodedata

from django.contrib.auth import get_user_model
from django.core.validators import ValidationError
from django.db.models import Q
from django.utils.encoding import force_str
from djoser.conf import settings

# Magic number 7: if you run into collisions with this number, then you are
# of big enough scale to start investing in a decent user model...
MAX_USERNAME_SUFFIX_LENGTH = 7
USERNAME_SUFFIX_CHARS = (
    [string.digits] * 4 +
    [string.ascii_letters] * (MAX_USERNAME_SUFFIX_LENGTH - 4))


def _generate_unique_username_base(txts, regex=None):
    username = None
    regex = regex or r'[^\w\s@+.-]'
    for txt in txts:
        if not txt:
            continue
        username = unicodedata.normalize('NFKD', force_str(txt))
        username = username.encode('ascii', 'ignore').decode('ascii')
        username = force_str(re.sub(regex, '', username).lower())
        # Django allows for '@' in usernames in order to accomodate for
        # project wanting to use e-mail for username. In allauth we don't
        # use this, we already have a proper place for putting e-mail
        # addresses (EmailAddress), so let's not use the full e-mail
        # address and only take the part leading up to the '@'.
        username = username.split('@')[0]
        username = username.strip()
        username = re.sub(r'\s+', '_', username)

    return username or settings.USERNAME_PREFIX


def get_username_max_length():
    if settings.LOGIN_FIELD is not None:
        User = get_user_model()
        max_length = User._meta.get_field(settings.LOGIN_FIELD).max_length
    else:
        max_length = 0

    return max_length


def generate_username_candidate(basename, suffix_length):
    max_length = get_username_max_length()
    suffix = ''.join(
        random.choice(USERNAME_SUFFIX_CHARS[i])
        for i in range(suffix_length))
    return basename[0:max_length - len(suffix)] + suffix


def generate_username_candidates(basename):
    if len(basename) >= settings.USERNAME_MIN_LENGTH:
        ret = [basename]
    else:
        ret = []
    min_suffix_length = max(1, settings.USERNAME_MIN_LENGTH - len(basename))
    max_suffix_length = min(
        get_username_max_length(),
        MAX_USERNAME_SUFFIX_LENGTH)
    for suffix_length in range(min_suffix_length, max_suffix_length):
        ret.append(generate_username_candidate(basename, suffix_length))
    return ret


def filter_users_by_username(*username):
    if settings.PRESERVE_USERNAME_CASING:
        qlist = [
            Q(**{settings.LOGIN_FIELD + '__iexact': u})
            for u in username]
        q = qlist[0]
        for q2 in qlist[1:]:
            q = q | q2
        ret = get_user_model().objects.filter(q)
    else:
        ret = get_user_model().objects.filter(
            **{settings.LOGIN_FIELD + '__in':
               [u.lower() for u in username]})
    return ret


def generate_unique_username(txts, regex=None):
    basename = _generate_unique_username_base(txts, regex)
    candidates = generate_username_candidates(basename)
    existing_usernames = filter_users_by_username(*candidates).values_list(
        settings.LOGIN_FIELD, flat=True)
    existing_usernames = set([n.lower() for n in existing_usernames])
    for candidate in candidates:
        if candidate.lower() not in existing_usernames:
            try:
                return candidate
            except ValidationError:
                pass
    # This really should not happen
    raise NotImplementedError('Unable to find a unique username')


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


class ListAsQuerySet(list):
    def __init__(self, *args, model, **kwargs):
        self.model = model
        super().__init__(*args, **kwargs)

    def filter(self, *args, **kwargs):
        return self  # filter ignoring, but you can impl custom filter

    def order_by(self, *args, **kwargs):
        return self
