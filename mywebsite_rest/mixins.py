# -*- coding: utf-8 -*-
"""
--------------------------------------------------------------
  Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.

  This Program may be used by anyone in accordance with the terms of the
  German Free Software License

  The License may be obtained under http://www.d-fsl.org.
--------------------------------------------------------------
"""

import locale

from django.conf import settings
from django.db import models
from django.utils.formats import localize_input, number_format, sanitize_separators
from django.utils.translation import activate, get_language_from_request

from parler_rest.fields import TranslatedField, TranslatedFieldsField
from rest_framework import serializers


class FlatTranslatedSerializerMixin(object):
    """
    Mixin for selecting only requested translation with django-parler-rest
    """

    def to_representation(self, instance):
        inst_rep = super().to_representation(instance)
        lang_code = None
        request = self.context.get('request')
        if request:
            lang_code = get_language_from_request(request)
            current_locale = locale.getlocale()[0]
            local_from_conf = settings.LANG_TO_LOCALE.get(lang_code)

        if lang_code:
            activate(lang_code)
            if current_locale != local_from_conf:
                locale.setlocale(locale.LC_ALL, local_from_conf)
        else:
            locale.setlocale(locale.LC_ALL, settings.LANG_TO_LOCALE_DEFAULT)
            activate(settings.LANGUAGE_CODE)

        result = {}
        for field_name, field in self.get_fields().items():
            # add normal field to resulting representation
            if not isinstance(field, (TranslatedField, TranslatedFieldsField)):
                field_value = inst_rep.pop(field_name)
                result.update({field_name: field_value})

            if isinstance(field, (TranslatedField, TranslatedFieldsField)):
                translations = inst_rep.pop(field_name)
                if lang_code not in translations:
                    # use fallback setting in PARLER_LANGUAGES
                    parler_default_settings = settings.PARLER_LANGUAGES['default']
                    if 'fallback' in parler_default_settings:
                        lang_code = parler_default_settings.get('fallback')

                    if 'fallbacks' in parler_default_settings:
                        translation_language_keys = [key for key in translations.keys()]
                        for parler_fallback in parler_default_settings.get('fallbacks'):
                            if parler_fallback in translation_language_keys:
                                lang_code = parler_fallback
                                break

                # if lang_code is still 'None', we have misconfigured something!
                for lang, translation_fields in translations.items():
                    if lang == lang_code:
                        trans_rep = translation_fields.copy()  # make copy to use pop() from
                        for trans_field_name, trans_field in translation_fields.items():
                            field_value = trans_rep.pop(trans_field_name)
                            result.update({trans_field_name: field_value})

        return result

    def to_internal_value(self, data):
        lang_code = None
        request = self.context.get('request')
        if request:
            lang_code = get_language_from_request(request)
            current_locale = locale.getlocale()[0]
            local_from_conf = settings.LANG_TO_LOCALE.get(lang_code)

        if lang_code:
            activate(lang_code)
            if current_locale != local_from_conf:
                locale.setlocale(locale.LC_ALL, local_from_conf)
        else:
            locale.setlocale(locale.LC_ALL, settings.LANG_TO_LOCALE_DEFAULT)
            activate(settings.LANGUAGE_CODE)

        # check if lang_code is configured in parler
        site_id = getattr(settings, 'SITE_ID', None)
        parler_languages = [parler_lang['code'] for parler_lang in settings.PARLER_LANGUAGES[site_id]]
        if lang_code not in parler_languages:
            # otherwise use fallback setting in PARLER_LANGUAGES
            parler_default_settings = settings.PARLER_LANGUAGES['default']
            if 'fallback' in parler_default_settings:
                lang_code = parler_default_settings.get('fallback')

            if 'fallbacks' in parler_default_settings:
                lang_code = parler_default_settings.get('fallbacks')[0]

        result = {}
        for field_name, field in self.get_fields().items():
            if not isinstance(field, (TranslatedField, TranslatedFieldsField)):
                if field_name in data:
                    field_value = data[field_name]
                    result.update({field_name: field_value})

            # rebuild translation field from meta data with values of flat representational request
            if isinstance(field, (TranslatedField, TranslatedFieldsField)):
                for meta in self.Meta.model._parler_meta:
                    if meta.rel_name == field_name:
                        # adding translations field wrapper for given lang_code to result
                        translations = {}
                        result.update({field_name: {lang_code: translations}})
                        for meta_field in meta.model._meta.get_fields():
                            # suppress adding fields from data, which are already added from non translation fields
                            if meta_field.name in data and meta_field.name not in result:
                                field_value = data[meta_field.name]
                                translations.update({meta_field.name: field_value})

        # serialize the result
        return super().to_internal_value(result)


class LocalizedNumbersSerializerMixin(object):
    """
    Mixin for retrieving localized numbers
    """

    def to_representation(self, instance):
        inst_rep = super().to_representation(instance)
        lang_code = None
        request = self.context.get('request')
        if request:
            lang_code = get_language_from_request(request)
            current_locale = locale.getlocale()[0]
            local_from_conf = settings.LANG_TO_LOCALE.get(lang_code)

        if lang_code:
            activate(lang_code)
            if current_locale != local_from_conf:
                locale.setlocale(locale.LC_ALL, local_from_conf)
        else:
            locale.setlocale(locale.LC_ALL, settings.LANG_TO_LOCALE_DEFAULT)
            activate(settings.LANGUAGE_CODE)

        result = {}
        for field_name, field in self.get_fields().items():
            field_value = inst_rep.pop(field_name)
            # change decimal and thousends separator for decimal field
            if not isinstance(field, (models.DecimalField, serializers.DecimalField)):
                result.update({field_name: field_value})
            else:
                result.update({field_name: number_format(
                    field_value, force_grouping=settings.USE_THOUSAND_SEPARATOR)})

        return result

    def to_internal_value(self, data):
        lang_code = None
        request = self.context.get('request')
        if request:
            lang_code = get_language_from_request(request)
            current_locale = locale.getlocale()[0]
            local_from_conf = settings.LANG_TO_LOCALE.get(lang_code)

        if lang_code:
            activate(lang_code)
            if current_locale != local_from_conf:
                locale.setlocale(locale.LC_ALL, local_from_conf)
        else:
            locale.setlocale(locale.LC_ALL, settings.LANG_TO_LOCALE_DEFAULT)
            activate(settings.LANGUAGE_CODE)

        result = {}
        for field_name, field in self.get_fields().items():
            if field_name in data:
                field_value = data[field_name]
                # change decimal and thousends separator for decimal field
                if isinstance(field, (models.DecimalField, serializers.DecimalField)):
                    field_value = localize_input(sanitize_separators(field_value))
                result.update({field_name: field_value})

        # serialize the result
        return super().to_internal_value(result)
