/**
 * Copyright (C) 2018 - 2022 dsoft-app-dev.de and friends.
 *
 * This Program may be used by anyone in accordance with the terms of the
 * German Free Software License
 *
 * The License may be obtained under http://www.d-fsl.org.
 */

// Dependencies
const path = require('path');
const webpack = require('webpack');
const BundleTracker = require('webpack-bundle-tracker');
const CleanWebpackPlugin = require('clean-webpack-plugin');
// const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WebpackAutoInject = require('webpack-auto-inject-version');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const marked = require('marked');
const renderer = new marked.Renderer();

module.exports = env => {
    const DEBUG = process.env.NODE_ENV !== 'production';
    const $dist = path.resolve(__dirname, 'dist');

    const config = {
        // The base directory (absolute path) for resolving the entry option.
        context: __dirname,
        mode: DEBUG ? 'development' : 'production',
        entry: {
            app: 'core_assets/js/app.js',
            appStyle: 'core_assets/js/appStyle.js',
            appAdmin: 'core_assets/js/appAdmin.js',
            appAdminStyle: 'core_assets/js/appAdminStyle.js',
        },
        cache: DEBUG,
        output: {
            path: path.join($dist, 'rest/assets_bundles'),
            filename: 'js/[name]-[hash].js',
            chunkFilename: 'js/[name]-[chunkhash].js',
            publicPath: DEBUG
                ? 'http://127.0.0.1:8080/dist/rest/assets_bundles/'
                : '/static/rest/assets_bundles/',
        },
        node: {
            fs: 'empty',
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    vendors: {
                        name: 'vendors',
                        test: /[\\/]node_modules[\\/]/,
                        priority: -10,
                        chunks: 'initial',
                    },
                    commons: {
                        name: 'commons',
                        test: /[\\/]?commons/,
                        enforce: true,
                        priority: -20,
                        chunks: 'all',
                        reuseExistingChunk: true,
                    },
                    default: {
                        name: 'shared',
                        reuseExistingChunk: true,
                    },
                },
            },
        },
        plugins: [
            new WebpackAutoInject({
                PACKAGE_JSON_PATH: './package.json',
                PACKAGE_JSON_INDENT: 2,
                components: {
                    AutoIncreaseVersion: true,
                    InjectAsComment: true,
                    InjectByTag: true,
                },
                componentsOptions: {
                    AutoIncreaseVersion: {
                        runInWatchMode: false, // it will increase version with every single build!
                    },
                    InjectAsComment: {
                        tag: 'Build version: {version} - {date}', // default
                        dateFormat: 'dddd, mmmm dS, yyyy, h:MM:ss TT', // default
                        multiLineCommentType: false, // default
                    },
                },
            }),
            new webpack.optimize.ModuleConcatenationPlugin(),
            new LodashModuleReplacementPlugin(),
            new MiniCssExtractPlugin({
                // Options similar to the same options in webpackOptions.output
                // both options are optional
                filename: 'css/[name]-[hash].css',
                chunkFilename: 'css/[name]-[chunkhash].css',
            }),
            // Where webpack stores data about bundles.
            new BundleTracker({
                filename:
                    './mywebsite_rest/static/mywebsite_rest/webpack-stats.json',
            }),
            // keep module.id stable when vender modules does not change
            new webpack.HashedModuleIdsPlugin(),
            // Makes jQuery available in every module.
            new webpack.ProvidePlugin({
                $: 'node_modules/jquery',
                jQuery: 'node_modules/jquery',
                'window.jQuery': 'node_modules/jquery',
                'window.$': 'node_modules/jquery',
                Popper: 'node_modules/popper.js/dist/umd/popper.min.js',
                JSONEditor: 'node_modules/jsoneditor/dist/jsoneditor.js',
                ScrollReveal: 'core_assets/js/default/scrollReveal.js',
            }),
            // Remove all files from the out dir, before re-creating.
            new CleanWebpackPlugin([`${$dist}/**/*`]),
            // Copy all image files from assets to webpack
            // new CopyWebpackPlugin([{
            //     from: './test_website/assets/img',
            //     to: path.join($dist, 'assets_bundles/test_website/assets/img')
            // }, ]),
            new CompressionPlugin({
                // filename: '[path][base].gz[query]',
                algorithm: 'gzip',
                test: /\.(js|jsx|css|html|png|svg|jpg|gif|woff|woff2|eot|ttf)$/i,
                exclude: /.map$/,
                threshold: 1,
                minRatio: 1, // Compress all files
                // deleteOriginalAssets: 'keep-source-map',
            }),
        ],
        module: {
            rules: [
                {
                    // Exposes jQuery for use outside Webpack build
                    test: require.resolve('jquery'),
                    use: [
                        {
                            loader: 'expose-loader',
                            options: 'jQuery',
                        },
                        {
                            loader: 'expose-loader',
                            options: '$',
                        },
                    ],
                },
                {
                    test: /\.(sa|sc)ss$/i,
                    use: [
                        DEBUG ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'postcss-loader',
                        'resolve-url-loader',
                        'sass-loader',
                    ],
                },
                {
                    test: /\.css$/i,
                    use: [
                        DEBUG ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'postcss-loader',
                    ],
                },
                {
                    test: /\.less$/i,
                    use: [
                        DEBUG ? 'style-loader' : MiniCssExtractPlugin.loader,
                        'css-loader',
                        'resolve-url-loader',
                        'less-loader',
                    ],
                },
                {
                    // Use loaders on all .js and .jsx files.
                    test: /\.jsx?$/i,
                    exclude: /(node_modules)/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    },
                },
                {
                    test: /\.json$/i,
                    exclude: /(node_modules)/,
                    loader: 'json-loader', //JSON loader
                },
                {
                    test: /\.txt$/i,
                    loader: 'raw-loader',
                },
                {
                    test: /\.(png|woff|woff2|svg|eot|ttf|gif|jpe?g)?(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                limit: 1000,
                                // for ManifestStaticFilesStorage reuse, use [path][name].[md5:hash:hex:12].[ext]
                                name: '[path][name].[ext]',
                            },
                        },
                        // {
                        //     loader: "image-webpack-loader",
                        //     options: {
                        //         query: {
                        //             bypassOnDebug: "true",
                        //             mozjpeg: {
                        //                 progressive: true
                        //             },
                        //             gifsicle: {
                        //                 interlaced: true
                        //             },
                        //             optipng: {
                        //                 optimizationLevel: 7
                        //             }
                        //         }
                        //     }
                        // }
                    ],
                },
                {
                    test: /\.md$/i,
                    use: [
                        {
                            loader: 'html-loader',
                        },
                        {
                            loader: 'markdown-loader',
                            options: {
                                pedantic: true,
                                renderer,
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            // extensions: ['.js', '.jsx'],
            modules: ['mywebsite_rest/assets', 'node_modules'],
            alias: {
                jquery: require.resolve('jquery'),
                node_modules: path.join(__dirname, 'node_modules'),
                core_assets: path.join(
                    __dirname,
                    'mywebsite_rest/static/mywebsite'
                ),
                rest_assets: path.join(__dirname, 'mywebsite_rest/assets'),
                static: path.join(__dirname, 'mywebsite_rest/static'),
            },
        },
        devServer: {
            writeToDisk: true,
            headers: {
                'Access-Control-Allow-Origin': '*',
            },
        },
    };

    // config.module.rules.unshift({
    //     parser: {
    //         amd: false,
    //     }
    // });

    return config;
};
